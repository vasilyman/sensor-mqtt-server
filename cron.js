const cron = require('node-cron');
module.exports.init = init;

function init(onSecond, onMinute, onHour, onDay) {
    // every second
    cron.schedule('* * * * * *', onSecond);
    // every minute
    cron.schedule('* * * * *', onMinute);
    // everu hour calc median
    cron.schedule('0 * * * *', onHour);
    // every day calc median
    cron.schedule('0 0 * * *', onDay);
}