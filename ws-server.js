const WebSocket = require('ws');
const Promise = require('promise');
const WebSocketServer = WebSocket.WebSocketServer;
const redis = require('./redis-client');
module.exports.init = init;

var wss;
var redisInstance;

function init(redisInst) {
  redisInstance = redisInst;
  wss = new WebSocketServer({ port: 3006, path: '/ws' }, () => {
    console.log('ws listening on port 3006');
  });

  wss.on('connection', function connection(ws, req) {
    console.log('ws connected', req.socket.remoteAddress);
  });

  wss.on('error', function err(error) {
    console.log('ws error', error);
  });

  return { wss, onMinute, onHour, onDay, onCurrent, onSecond };
}

function cast(data) {
  if (!('clients' in wss)) return;

  wss.clients.forEach(function each(ws) {
    if (ws.readyState === WebSocket.OPEN) {
      ws.send(JSON.stringify(data));
    }
  });
};

function onCurrent(data) {
  cast({ type: 'current', data });
}

async function readAndCast(type) {
  if (redisInstance.redisClient && redisInstance.redisClient.connected) {
    const key = `sensors:test:1:${type}`;
    const [data] = await redisInstance.readSensors(key, 0, 0);
    cast({ type, data });
  }
}

function onSecond() {
  readAndCast('second');
}

function onMinute() {
  readAndCast('minute');
}

function onHour() {
  readAndCast('hour');
}

function onDay() {
  readAndCast('day');
}