const express = require('express');
const cors = require('cors');
const Promise = require('promise');

module.exports.init = init;

const app = express();
const port = 3004;
const allowlist = ['localhost', 'http://localhost'];

const corsOptionsDelegate = function (req, callback) {
    var corsOptions;
    if (allowlist.indexOf(req.header('Origin')) !== -1) {
        corsOptions = { origin: true } // reflect (enable) the requested origin in the CORS response
    } else {
        corsOptions = { origin: false } // disable CORS for this request
    }
    callback(null, corsOptions) // callback expects two parameters: error and options
}

function init(redisInstance) {
    app.get('/api/sensors/1', cors(corsOptionsDelegate), async (req, res) => {
        const type = req.query.type || 'current';
        const data = await redisInstance.readSensors(`sensors:test:1:${type}`)
            .catch((err) => {
                console.log(err);
            });
        res.json(data.reverse());
    });

    app.listen(port, () => {
        console.log(`Example app listening at http://localhost:${port}`);
    });
}