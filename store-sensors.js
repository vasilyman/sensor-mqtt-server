const mqtt = require('./mqtt-client');
module.exports.init = init;

var mqttClient, redisClient, redisPush;

function init(redis) {
    mqttClient = mqtt.init(onConnect, onMessage, onError);
    redisClient = redis.redisClient;
    redisPush = redis.redisPush;
}

function onConnect() {
    console.log('connected!');
}

function parseData(message) {
    var keys = ['co2', 'tVOC', 'pres', 'temp', 'hum', 'timedelta'];
    var result = {};
    keys.forEach((key, i) => {
        try {
            result[key] = Number(message.subarray(i * 8, i * 8 + 8).readDoubleLE(0).toFixed(2));
        } catch (error) {
            // console.log(error);
        }
    });
    result.timestamp = Date.now();
    return result;
}

function onMessage(topic, message) {
    var result = parseData(message);
    if (typeof redisPush === 'function') redisPush(result);
}

function onError() {
    console.log('err');
}