const redis = require("redis");
const Promise = require('promise');
module.exports = {
  init,
  connect,
};

const host = '127.0.0.1';
const port = '6379';

const options = {
  host,
  port,
};

var redisClient;
var redisSubscriber;
var onCurrent;

function connect() {
  return redis.createClient(options);
}

function init(onCurrentCb) {
  onCurrent = onCurrentCb;
  redisClient = connect();
  redisSubscriber = connect();

  subscribeToCurrent();

  redisClient.on("error", function (error) {
    onRedisError(error);
  });

  redisClient.on("connect", function () {
    onRedisConnect();
  });

  return { redisClient, redisPush, readSensors, onMinute, onHour, onDay, onSecond };
}

function redisPush(data) {
  if (redisClient.connected) {
    const payload = JSON.stringify(data);
    redisClient.publish('sensors:test:1:current', payload);
    redisClient.lpush('sensors:test:1:current', payload);
    redisClient.ltrim('sensors:test:1:current', 0, 99);
  }
}

function onRedisConnect() {
  console.log('redis connected');
}

function onRedisError(error) {
  console.log(error);
}

async function readSensors(key, start, end) {
  return new Promise((resolve, reject) => {
    var data = [];
    try {
      if (redisClient.connected) {
        redisClient.lrange(key, (start || 0), (end || -1), (err, val) => {
          if (!err) {
            data = val.map((v) => JSON.parse(v));
            resolve(data);
          } else {
            reject(err)
          }
        });
      } else resolve(data);
    } catch (error) {
      reject(error);
    }
  });
}

async function subscribeToCurrent(ws) {
  const topic = 'sensors:test:1:';
  redisSubscriber.on('pmessage', (pattern, channel, data) => {
    const r = new RegExp(`^${topic}(.*)$`);
    const type = channel.match(r)[1];
    if (!type) return;
    if (type === 'current' && typeof onCurrent === 'function') onCurrent(JSON.parse(data));
  });
  redisSubscriber.psubscribe(topic + '*');
}

async function getAllSensors() {
  const sensors = ['sensors:test:1:current'];
  const promises = [];
  sensors.forEach((key) => {
    promises.push(readSensors(key));
  });
  const result = await Promise.all(promises);
  return result;
}

function getAvgSensors(data) {
  const keys = Object.keys(data[0]);
  const result = {};
  keys.forEach((key) => {
    const values = data.map((item) => Number(item[key])).filter((val) => !Number.isNaN(val));
    if (key === 'timestamp') {
      result[key] = Date.now();
     } else result[key] = Number((values.reduce((a, b) => a + b, 0) / values.length).toFixed(2));
  });
  return result;
}

async function writeMedian(prev, next, length) {
  if (redisClient.connected) {
    const key = prev;
    const data = await readSensors(key, 0, length-1);

    const payload = JSON.stringify(getAvgSensors(data));
    redisClient.lpush(next, payload);
    redisClient.ltrim(next, 0, 99);
  }
}

// calc median for each sensor
function onSecond() {
  writeMedian('sensors:test:1:current', 'sensors:test:1:second', 7);
}

// calc median for each sensor
function onMinute() {
  writeMedian('sensors:test:1:second', 'sensors:test:1:minute', 60);
}

function onHour() {
  writeMedian('sensors:test:1:minute', 'sensors:test:1:hour', 60);
}

function onDay() {
  writeMedian('sensors:test:1:hour', 'sensors:test:1:day', 24);
}