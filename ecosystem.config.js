module.exports = {
    apps: [{
      name: 'Sensors-backend',
      script: 'npm',
  
      // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
      args: 'run start',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '256M',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    }],
  };
  