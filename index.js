const redis = require('./redis-client');
const sensors = require('./store-sensors');
const server = require('./server');
const cron = require('./cron');
const ws = require('./ws-server');

const redisInited = redis.init(onCurrent);

sensors.init(redisInited);
server.init(redisInited);
const wsInited = ws.init(redisInited);

function onSecond() {
    redisInited.onSecond();
    wsInited.onSecond();
}

function onMinute() {
    redisInited.onMinute();
    wsInited.onMinute();
}

function onHour() {
    redisInited.onHour();
    wsInited.onHour();
}

function onDay() {
    redisInited.onDay();
    wsInited.onDay();
}

function onCurrent(data) {
    if (typeof wsInited.onCurrent === 'function') wsInited.onCurrent(data);
}

cron.init(onSecond, onMinute, onHour, onDay);